﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private Transform _enemyParentObjectTransform;

    [SerializeField]
    private Enemy _enemyInstance;

    [SerializeField]
    private WorldBlock _startBlock;

    public WorldBlock StartBlock { get => _startBlock; }

    [SerializeField]
    private WorldBlock _endBlock;
    public WorldBlock EndBlock { get => _endBlock; }

    [SerializeField]
    private float _spawnDelay = 1.0f;

    [SerializeField]
    private float _maxEnemies = 10.0f;
    private float _spawnedEnemies = 0.0f;




    void Start()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            float totalEnemies = GameObject.FindObjectsOfType<Enemy>().Length;

            if (totalEnemies < _maxEnemies)
            {
                Enemy enemy = Instantiate<Enemy>(_enemyInstance, transform);
                enemy.SetStartBlock(_startBlock);
                enemy.SetEndBlock(_endBlock);
                enemy.transform.localScale = new Vector3(3.0f, 3.0f, 3.0f);
                enemy.transform.parent = _enemyParentObjectTransform;
                enemy.tag = "Enemy";
                enemy.name = "Enemy " + _spawnedEnemies;
                enemy.HitPoints = 50.0f;
                _spawnedEnemies++;
            }

            yield return new WaitForSeconds(_spawnDelay);
        }
    }

}
