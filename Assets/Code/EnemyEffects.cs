﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEffects : MonoBehaviour
{
    private Transform _hierarchyParentTransform;

    [Header("Effects")]
    [SerializeField]
    private ParticleSystem _explosion = null;

    [SerializeField]
    private ParticleSystem _deathExplosion = null;

    private void Awake()
    {
        _hierarchyParentTransform = GameObject.Find("VFX").transform;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayExplosion()
    {
        if (_explosion != null)
        {
            if (_explosion.isPlaying)
            {
                _explosion.Stop();
            }

            _explosion.Play();
        }
    }

    public void OnDeath()
    {
        var vfx = Instantiate(_deathExplosion, _deathExplosion.transform);
        vfx.transform.parent = _hierarchyParentTransform;
        
        vfx.Play();

        Destroy(gameObject);

    }
}
