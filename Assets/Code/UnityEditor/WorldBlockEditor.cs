﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(WorldBlock))]
public class WorldBlockEditor : MonoBehaviour
{
    private TextMesh _textMesh;
    private WorldGrid _worldGrid;
    private WorldBlock _worldBlock;

    void Start()
    {
        _textMesh = GetComponentInChildren<TextMesh>();
        _worldGrid = GetComponentInParent<WorldGrid>();
        _worldBlock = GetComponent<WorldBlock>();
    }

    // Update is called once per frame
    void Update()
    {
        SnapPositionInEditor();
        
        if(_textMesh != null)
        {
            UpdateLabel();
        }
    }

    private void SnapPositionInEditor()
    {
        Vector3 snapPos;
        float gridSize = _worldGrid.GetGridSize();

        snapPos.x = _worldGrid.IsXLocked() ? 0.0f : Mathf.Floor(transform.position.x / gridSize) * gridSize;
        snapPos.y = _worldGrid.IsYLocked() ? 0.0f : Mathf.Floor(transform.position.y / gridSize) * gridSize;
        snapPos.z = _worldGrid.IsZLocked() ? 0.0f : Mathf.Floor(transform.position.z / gridSize) * gridSize;

        transform.position = snapPos;
    }

    private void UpdateLabel()
    {
        _textMesh.text = _worldBlock.ToString();
        gameObject.name = _worldBlock.ToString();
    }
}
