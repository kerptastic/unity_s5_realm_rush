﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private bool _isPaused = false;

    [SerializeField]
    private float _moveSpeed = 5.0f;

    private Pathfinder _pathFinder;
    private bool _movingToTarget = false;

    private Stack<WorldBlock> _path = null;
    private WorldBlock _wb = null;


    // Start is called before the first frame update
    void Start()
    {
        _pathFinder = GetComponent<Pathfinder>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_path == null)
        {
            _path = _pathFinder.FindPath();
        }

        if (_wb == null && _path.Count > 0)
        {
            _wb = _path.Pop();
        }

        Vector3 endBlockPos = _pathFinder.EndBlock.transform.position;

        // TODO - need to add the Y to the endblock position for this to be equal fully
        if (transform.position.x != endBlockPos.x || transform.position.z != endBlockPos.z)
        {
            MoveToTarget(_wb);
        }
    }

    private void MoveToTarget(WorldBlock wb)
    {
        float step = _moveSpeed * Time.deltaTime;

        Vector3 dest = wb.transform.position;
        dest.y += transform.position.y;

        transform.position = Vector3.MoveTowards(transform.position, dest, step);

        if (transform.position == dest)
        {
            _wb = null;
        }
    }
}
