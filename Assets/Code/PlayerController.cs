﻿using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private PlayerInput _playerInput = null;

    void OnEnable() => _playerInput.Enable();

    void OnDisable() => _playerInput.Disable();

    private void Awake()
    {

        _playerInput = new PlayerInput();
        _playerInput.Player.WorldBlockSelect.performed += ctx => Clicked(ctx);
    }


    void Clicked(InputAction.CallbackContext ctx)
    {
        //_isSelected = ctx.ReadValue<bool>();

        print("CLICKED");
    }



}
