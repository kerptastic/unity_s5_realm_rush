﻿using UnityEngine;
using UnityEngine.InputSystem;

[ExecuteInEditMode]
public class WorldBlock : MonoBehaviour
{
    [SerializeField]
    private Vector2Int _gridPosition = new Vector2Int();
    public Vector2Int GridPosition
    {
        get
        {
            float gridSize = _worldGrid.GetGridSize();

            _gridPosition.x = Mathf.FloorToInt(transform.position.x / gridSize);
            _gridPosition.y = Mathf.FloorToInt(transform.position.z / gridSize);

            return _gridPosition;
        }
    }

    [SerializeField]
    private bool _isBuildable = false;
    public bool IsBuildable { get => _isBuildable; set => _isBuildable = value; }

    [SerializeField]
    private bool _isTraversable = true;
    public bool IsTraversable { get => _isTraversable; set => _isTraversable = value; }

    [SerializeField]
    private bool _isSelected = false;
    public bool IsSelected { get => _isSelected; set => _isSelected = value; }

    [SerializeField]
    private Transform _towerParentObjectTransform = null;

    [SerializeField]
    private Tower _towerPrefab = null;
    private Tower _towerInstance = null;


    private Color _defaultTopColor;
    private Color _highlightTopColor = new Color(0.0f, 1.0f, 0.0f);

    private WorldGrid _worldGrid;


    void Awake()
    {
        _worldGrid = GetComponentInParent<WorldGrid>();

        MeshRenderer mr = transform.Find("Top").GetComponent<MeshRenderer>();
        _defaultTopColor = mr.sharedMaterial.color;
    }


    void OnMouseEnter()
    {
        SetTopColor(_highlightTopColor);
    }


    void OnMouseOver()
    {
        SetTopColor(_highlightTopColor);

        if (true)
        {

        }
    }

    void OnMouseDown()
    {
        print("click");

        // broke these into two just in case we wanted a different action if the tower
        // was already placed
        if (_towerInstance != null)
        {
            return;
        }

        if (IsBuildable)
        {
            _towerInstance = Instantiate<Tower>(_towerPrefab);

            _towerInstance.transform.localScale = new Vector3(3.0f, 3.0f, 3.0f);
            _towerInstance.transform.position = transform.position;
            _towerInstance.transform.parent = _towerParentObjectTransform;
            _towerInstance.name = "Tower - (" + ToString() + ")";

            _towerInstance.FireRate = 1.5f;
            _towerInstance.Range = 35.0f;
        }
    }

    void OnMouseExit()
    {
        SetTopColor(_defaultTopColor);
    }


    public Vector2Int GetGridPosition()
    {
        float gridSize = _worldGrid.GetGridSize();

        _gridPosition.x = Mathf.FloorToInt(transform.position.x / gridSize);
        _gridPosition.y = Mathf.FloorToInt(transform.position.z / gridSize);

        return GridPosition;
    }


    public void SetTopColor(Color c)
    {
        MeshRenderer mr = transform.Find("Top").GetComponent<MeshRenderer>();
        mr.material.color = c;
    }


    public override string ToString()
    {
        return GridPosition.x + ", " + GridPosition.y;
    }
}
