﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private float _damage = 10.0f;

    [SerializeField]
    private Transform _target = null;

    [SerializeField]
    private Vector3 _velocity = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;

        switch (other.tag)
        {
            case "Enemy":
                Enemy enemy = other.GetComponent<Enemy>();
                enemy.TakeDamage(_damage);
                Destroy(gameObject);

                break;
            default:
                print("Unknown Bullet Collision");
                break;
        };

        //Destroy(gameObject);
    }
}
