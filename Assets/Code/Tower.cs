﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class Tower : MonoBehaviour
{
    [SerializeField]
    Transform _target = null;

    [SerializeField]
    private GameObject _bullet = null;

    [SerializeField]
    private float _range = 25.0f;
    public float Range { get => _range; set => _range = value; }

    [SerializeField]
    private float _fireRate = 2.0f;
    public float FireRate { get => _fireRate; set => _fireRate = value; }

    [SerializeField]
    private bool _ceaseFire = false;

    private GameObject _bulletSpawn = null;
    private Vector3 _housingOffset = Vector3.zero;


    void Awake()
    {
        _bulletSpawn = transform.Find("BulletSpawn").gameObject;
    }

    void Start()
    {
        StartCoroutine(Fire());
    }

    // Update is called once per frame
    void Update()
    {
        SetTarget();

        if (_target != null)
        {
            transform.LookAt(_target);
        }
    }

    void SetTarget()
    {
        if (_target != null && IsTargetInRange(_target))
        {
            return;
        }

        // not resetting the _target to null initially due to condition where coroutine
        // for fire could end up with a null target
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        Enemy potentialTarget = GetClosestTarget(enemies);

        if (potentialTarget == null)
        {
            _target = null;
        }
        else
        {
            Transform potentialTargetTransform = potentialTarget.transform;

            if (IsTargetInRange(potentialTargetTransform))
            {
                _target = potentialTargetTransform;
            }
            else
            {
                _target = null;
            }
        }
    }

    private Enemy GetClosestTarget(Enemy[] enemies)
    {
        if (enemies == null || enemies.Length == 0)
        {
            return null;
        }

        Enemy closest = enemies[0];

        foreach (Enemy nextEnemy in enemies)
        {
            var distanceToNext = Vector3.Distance(transform.position, nextEnemy.transform.position);
            var distanceToCurrent = Vector3.Distance(transform.position, closest.transform.position);

            if (distanceToNext < distanceToCurrent)
            {
                closest = nextEnemy;
            }
        }

        return closest;
    }

    private bool IsTargetInRange(Transform targetTransform)
    {
        if (targetTransform == null)
        {
            return false;
        }

        return Vector3.Distance(transform.position, targetTransform.position) <= _range;
    }

    IEnumerator Fire()
    {
        while (true)
        {
            if (!_ceaseFire && _target != null && IsTargetInRange(_target))
            {
                GameObject clone;
                Rigidbody rigidBody;

                Vector3 newPosition = _bulletSpawn.transform.position;
                Quaternion newRotation = _bulletSpawn.transform.rotation;

                clone = Instantiate(_bullet, newPosition, newRotation);
                rigidBody = clone.GetComponent<Rigidbody>();

                if (rigidBody != null)
                {
                    rigidBody.velocity = transform.TransformDirection(Vector3.forward * 150.0f);
                }
            }

            yield return new WaitForSeconds(1.0f / _fireRate);
        }
    }
}
