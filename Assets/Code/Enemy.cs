﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float _hitPoints = 100.0f;
    public float HitPoints { get => _hitPoints; set => _hitPoints = value; }

    private Pathfinder _pathfinder = null;

    private void Awake()
    {
        _pathfinder = GetComponent<Pathfinder>();
    }

    public void SetStartBlock(WorldBlock wb)
    {
        _pathfinder.StartBlock = wb;
    }

    public void SetEndBlock(WorldBlock wb)
    {
        _pathfinder.EndBlock = wb;
    }

    public void TakeDamage(float amount)
    {
        _hitPoints -= amount;
        SendMessage("PlayExplosion");

        if (_hitPoints <= 0.0f)
        {
            SendMessage("OnDeath");
        }
    }
}
