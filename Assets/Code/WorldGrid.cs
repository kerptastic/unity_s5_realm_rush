﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WorldGrid : MonoBehaviour
{
    [SerializeField]
    private float _gridSize = 1.0f;

    [SerializeField]
    private bool _lockX = false;

    [SerializeField]
    private bool _lockY = false;

    [SerializeField]
    private bool _lockZ = false;

    private Dictionary<Vector2Int, WorldBlock> _gridPositionToBlockMap = new Dictionary<Vector2Int, WorldBlock>();

    void Start()
    {
        LoadWorldBlocks();
    }

    private void LoadWorldBlocks()
    {
        var worldBlocks = GetComponentsInChildren<WorldBlock>();

        foreach (WorldBlock wb in worldBlocks)
        {
            if (_gridPositionToBlockMap.ContainsKey(wb.GridPosition))
            {
                Debug.LogWarning("Overlapped Block: " + wb.ToString());
            }
            else
            {
                _gridPositionToBlockMap.Add(wb.GridPosition, wb);
            }
        }

        print("Loaded " + _gridPositionToBlockMap.Count + " World Blocks.");
    }

    public WorldBlock GetWorldBlockAtPosition(Vector2Int position)
    {
        _gridPositionToBlockMap.TryGetValue(position, out WorldBlock block);
        return block;
    }

    public Dictionary<Vector2Int, WorldBlock>.ValueCollection GetWorldBlocks()
    {
        return _gridPositionToBlockMap.Values;
    }

    public bool HasWorldBlockAtPosition(Vector2Int position)
    {
        return _gridPositionToBlockMap.ContainsKey(position);
    }

    public float GetGridSize()
    {
        return _gridSize;
    }

    public bool IsXLocked()
    {
        return _lockX;
    }

    public bool IsYLocked()
    {
        return _lockY;
    }

    public bool IsZLocked()
    {
        return _lockZ;
    }
}
