﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pathfinder : MonoBehaviour
{
    public enum MoveDirection
    {
        UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT,
    }

    private static Dictionary<MoveDirection, Vector2Int> _moveDirectionMapping = new Dictionary<MoveDirection, Vector2Int>()
    {

        { MoveDirection.UP_LEFT, new Vector2Int(-1, 1) },
        { MoveDirection.UP, Vector2Int.up },
        { MoveDirection.UP_RIGHT, new Vector2Int(1, 1) },
        { MoveDirection.LEFT, Vector2Int.left },
        { MoveDirection.RIGHT, Vector2Int.right },
        { MoveDirection.DOWN_LEFT, new Vector2Int(-1, -1) },
        { MoveDirection.DOWN, Vector2Int.down },
        { MoveDirection.DOWN_RIGHT, new Vector2Int(1, -1) },
    };


    [SerializeField]
    private bool _isColoringBlocks = false;

    [SerializeField]
    private Color _startBlockColor;
    [SerializeField]
    private Color _endBlockColor;
    [SerializeField]
    private Color _defaultBlockColor;
    [SerializeField]
    private Color _pathColor;
    [SerializeField]
    private Color _nonTraverseColor;

    [SerializeField]
    private WorldBlock _startBlock;
    public WorldBlock StartBlock { get => _startBlock; set => _startBlock = value; }

    [SerializeField]
    private WorldBlock _endBlock;
    public WorldBlock EndBlock { get => _endBlock; set => _endBlock = value; }

    // member variables
    private WorldGrid _worldGrid;

    private Queue<WorldBlock> _pathfindingQueue = new Queue<WorldBlock>();
    private Dictionary<Vector2Int, Vector2Int> _traversesToFrom = new Dictionary<Vector2Int, Vector2Int>();


    // Start is called before the first frame update
    void Start()
    {
        _worldGrid = FindObjectOfType<WorldGrid>();

        //if (_isColoringBlocks)
        //{
        //    ReinitializeBlockColors();
        //}
    }

    // Update is called once per frame
    void Update()
    {
        var thePath = FindPath();

        if (_isColoringBlocks)
        {
            ReinitializeBlockColors();

            if (thePath.Count > 0)
            {
                ColorThePath(thePath);
            }
        }
    }

    public Stack<WorldBlock> FindPath()
    {
        Stack<WorldBlock> thePath = new Stack<WorldBlock>();
        _traversesToFrom.Clear();
        _pathfindingQueue.Clear();

        // put the end block in first in the stack (the final block)
        thePath.Push(EndBlock);

        // start on the first block, and then explore the grid for the destination
        _pathfindingQueue.Enqueue(StartBlock);
        _traversesToFrom.Add(StartBlock.GetGridPosition(), StartBlock.GetGridPosition());

        while (_pathfindingQueue.Count > 0)
        {
            var currentBlock = _pathfindingQueue.Dequeue();

            // check if we are done, if not - work on the neighbors and go deeper
            if (currentBlock == EndBlock)
            {
                var examinedGridPosition = EndBlock.GetGridPosition();

                // walk backwards through the traversal map adding the items to the path
                // using a stack here since they will be popped off in LIFO (the proper order)
                while (examinedGridPosition != StartBlock.GetGridPosition())
                {
                    thePath.Push(_worldGrid.GetWorldBlockAtPosition(examinedGridPosition));
                    _ = _traversesToFrom.TryGetValue(examinedGridPosition, out examinedGridPosition);
                }

                // add the start block, which doesnt get added due to while loop condition
                thePath.Push(StartBlock);
                break;
            }

            EnqueueNeighbors(currentBlock);
        }

        // if there is just one element, its just the end block - we didnt find anything
        if (thePath.Count == 1)
        {
            thePath.Clear();
        }

        return thePath;
    }

    private void EnqueueNeighbors(WorldBlock wb)
    {
        foreach (Vector2Int key in _moveDirectionMapping.Values)
        {
            Vector2Int possibleNeighborGridPosition = new Vector2Int
            {
                x = wb.GetGridPosition().x + key.x,
                y = wb.GetGridPosition().y + key.y
            };

            if (IsVisitable(possibleNeighborGridPosition))
            {
                var neighbor = _worldGrid.GetWorldBlockAtPosition(possibleNeighborGridPosition);

                _pathfindingQueue.Enqueue(neighbor);
                _traversesToFrom.Add(neighbor.GetGridPosition(), wb.GetGridPosition());
            }
        }
    }

    private bool IsVisitable(Vector2Int gridLocation)
    {
        if (!_worldGrid.HasWorldBlockAtPosition(gridLocation) || _traversesToFrom.ContainsKey(gridLocation))
        {
            return false;
        }

        return _worldGrid.GetWorldBlockAtPosition(gridLocation).IsTraversable;
    }


    private void ReinitializeBlockColors()
    {
        foreach (WorldBlock wb in _worldGrid.GetWorldBlocks())
        {
            SetBlockPathFindingColor(wb);
        }
    }

    private void SetBlockPathFindingColor(WorldBlock wb)
    {
        // if (wb.Equals(StartBlock))
        // {
        //     wb.SetTopColor(_startBlockColor);
        // }
        // else if (wb.Equals(EndBlock))
        // {
        //     wb.SetTopColor(_endBlockColor);
        // }
        // else if (!wb.IsTraversable)
        // {
        //     wb.SetTopColor(_nonTraverseColor);
        // }
        // else
        // {
        //     wb.SetTopColor(_defaultBlockColor);
        // }
    }

    private void ColorThePath(Stack<WorldBlock> thePath)
    {
        // foreach (var wb in thePath)
        // {
        //     if (wb != StartBlock && wb != EndBlock)
        //     {
        //         wb.SetTopColor(_pathColor);
        //     }
        // }
    }
}
